import '../scss/styles.scss'

// Плавное уменьшение header при скролле
window.addEventListener("scroll", () => {
    if (window.scrollY > 1) {
        document.querySelector(".header").classList.add("header_sticky");
    } else {
        document.querySelector(".header").classList.remove("header_sticky");
    }
});

let firstParent = document.querySelector(".tab-content:nth-child(3)");
let secondParent = document.querySelector(".tab-content:nth-child(4)");
let firstImg = document.getElementById("first-img");
let secondImg = document.getElementById("second-img");

// Drag'n Drop
firstImg.onmousedown = function(e) {
    let coords = getCoords(firstImg);
    let shiftX = e.pageX - coords.left;
    let shiftY = e.pageY - coords.top;

    firstImg.style.zIndex = 1000;

    function moveAt(e) {
        firstImg.style.left = e.pageX - shiftX + "px";
        firstImg.style.top = e.pageY - shiftY + "px";
    }

    firstParent.onmousemove = function(e) {
        moveAt(e);
    };

    firstParent.onmouseout = function() {
        firstParent.onmousemove = null;
        firstImg.onmouseup = null;
    };

    firstImg.onmouseup = function() {
        firstParent.onmousemove = null;
        firstImg.onmouseup = null;
    };
};

firstImg.ondragstart = function() {
    return false;
};

function getCoords(elem) {
    return {
        top: elem.offsetTop,
        left: elem.offsetLeft
    };
}

// Плавное перемещение изображения
document.querySelector(".quarter-topleft").addEventListener("mouseover", () => {
    secondImg.style.right =
        -(secondImg.offsetWidth - secondParent.offsetWidth) + "px";
    secondImg.style.bottom =
        -(secondImg.offsetHeight - secondParent.offsetHeight) + "px";
});
document
    .querySelector(".quarter-topright")
    .addEventListener("mouseover", () => {
        secondImg.style.right = 0 + "px";
        secondImg.style.bottom =
            -(secondImg.offsetHeight - secondParent.offsetHeight) + "px";
    });
document.querySelector(".quarter-botleft").addEventListener("mouseover", () => {
    secondImg.style.right =
        -(secondImg.offsetWidth - secondParent.offsetWidth) + "px";
    secondImg.style.bottom = 0 + "px";
});
document
    .querySelector(".quarter-botright")
    .addEventListener("mouseover", () => {
        secondImg.style.right = 0 + "px";
        secondImg.style.bottom = 0 + "px";
    });

// DropDown меню
let dropdownHeader = document.querySelector(".dropdown-header");
let activeTabHeader = dropdownHeader.querySelector(".active-tab-header");
let tabHeaderWrap = document.querySelector(".tab-header-wrap");
let tabHeaders = tabHeaderWrap.querySelectorAll(".js-tab-header");
let activeTab = tabHeaderWrap.querySelector(".active");
let arrow = dropdownHeader.querySelector(".arrow");

activeTabHeader.textContent = activeTab.textContent;

if(window.matchMedia('(max-width: 768px)').matches){    

    document.querySelector(".main").addEventListener("click", () => {
        if (!dropdownHeader.contains(event.target)) {
            tabHeaderWrap.style.height = 0 + "px";
            arrow.classList.remove("arrow_dropped");
        }
    });

    dropdownHeader.addEventListener("click", () => {
        if (!arrow.classList.contains("arrow_dropped")) {
            tabHeaderWrap.style.height = tabHeaders.length * 40 + 50 + "px";
            setTimeout(function() {
                tabHeaderWrap.style.height = tabHeaders.length * 40 + "px";
            }, 500);
            arrow.classList.add("arrow_dropped");
        } else {
            tabHeaderWrap.style.height = 0 + "px";
            arrow.classList.remove("arrow_dropped");
        }
    });

    tabHeaders.forEach(elem => {
        elem.addEventListener("click", () => {
            tabHeaderWrap.style.height = 0 + "px";
            setTimeout(function(){
                activeTab = tabHeaderWrap.querySelector(".active");
                activeTabHeader.textContent = activeTab.textContent;
            }, 500);            
            arrow.classList.remove("arrow_dropped");
        });
    });
}