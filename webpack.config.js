const path = require('path');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const fs = require('fs');

const scriptsDir = "./src/js/";

const scripts = fs.readdirSync(scriptsDir).filter(elem => elem.endsWith('.js')).map(elem => scriptsDir + elem);

module.exports = {
    entry: {
      vendor: './src/js/vendor/vendor.js',
      scripts: scripts,
    },
    output: {
        path: path.resolve(__dirname, "./dist/"),
        filename: "./js/[name].min.js"
    },

    devtool: "source-map",

    devServer: {
        overlay: true
    },

    module: {
        rules: [
            {
                test: /\.js$/,
                loader: "babel-loader",
                exclude: /node_modules/
            },

            {
                test: /\.scss$/,
                use: [
                    {
                        loader: MiniCssExtractPlugin.loader,
                        options: { sourceMap: true, publicPath: "dist" }
                    },
                    { loader: "css-loader", options: { sourceMap: true } },
                    { loader: "sass-loader", options: { sourceMap: true } }
                ]
            }
        ]
    },

    plugins: [
        new MiniCssExtractPlugin({
            filename: "./css/styles.min.css"
        }),
        new HtmlWebpackPlugin({
            template: `./src/index.html`,
            filename: `index.html`
        })
    ]
};
